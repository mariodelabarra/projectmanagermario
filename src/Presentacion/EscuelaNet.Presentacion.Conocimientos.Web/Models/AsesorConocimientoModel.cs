﻿using EscuelaNet.Aplicacion.Conocimiento.QueryModels;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class AsesorConocimientoModel
    {
        public ConocimientoQueryModel Conocimiento { get; set; }
        public List<ConocimientoQueryModel> Conocimientos { get; set; }
        public AsesorQueryModel Asesor { get; set; }
        public List<AsesorQueryModel> Asesores { get; set; }
    }
}