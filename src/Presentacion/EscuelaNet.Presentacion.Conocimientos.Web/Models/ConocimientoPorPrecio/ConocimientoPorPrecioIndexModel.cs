﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class ConocimientoPorPrecioIndexModel
    {
        public string Nombre { get; set; }
        public List<ConocimientoPorPrecio> ConocimientoPorPrecios { get;set;}
    }
}