﻿using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices.CapacitacionServices;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorTemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionTemaCommand;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class CapacitacionesController : Controller
    {
        // GET: Capacitaciones
        private ICapacitacionRepository _capacitacionRepository;
        private ICapacitacionesQuery _capacitacionesQuery;
        private ILugaresQuery _lugaresQuery;
        private IMediator _mediator;
        private ITemasQuery _temasQuery;

  
        public CapacitacionesController(ICapacitacionRepository capacitacionRepository, ICapacitacionesQuery capacitacionesQuery,
            IMediator mediator,ILugaresQuery lugaresQuery,ITemasQuery temasQuery)
        {
            _capacitacionRepository = capacitacionRepository;
            _capacitacionesQuery = capacitacionesQuery;
            _lugaresQuery = lugaresQuery;
            _mediator = mediator;
            _temasQuery = temasQuery;

        }
        // GET: capa
        public ActionResult Index()
        {
            var capacitaciones = _capacitacionesQuery.ListCapacitaciones();

            var model = new CapacitacionIndexModel()
            {
                Titulo = "Lugares",
                Capacitaciones = capacitaciones
            };

            return View(model);
        }
        public ActionResult New()
        {
            var lugares = _lugaresQuery.ListLugares();
            var model = new NuevoCapacitacionModel()
            {
                Duracion = 0,
                Maximo = 0,
                Minimo = 0,
                Precio = 0,
                Inicio = DateTime.Now,
                Fin = DateTime.Now,
                Lugares = lugares
            };
            return View(model);
        }

        // POST: capacitaciones/New
        [HttpPost]
        public async Task<ActionResult> New(NuevoCapacitacionCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Capacitacion Creada";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoCapacitacionModel()
                {
                    IDLugar = model.IDLugar,
                    Minimo = model.Minimo,
                    Maximo = model.Maximo,
                    Duracion = model.Duracion,
                    Precio = model.Precio,
                    Inicio = model.Inicio,
                    Fin = model.Fin,
                };
                return View(modelReturn);
            }

        }

        public ActionResult Edit(int id)
        {
            var capacitacion = _capacitacionesQuery.GetCapacitacion(id);
            var lugares = _lugaresQuery.ListLugares();
            var model = new NuevoCapacitacionModel()
            {
                Duracion = capacitacion.Duracion,
                Maximo = capacitacion.Maximo,
                Minimo = capacitacion.Minimo,
                Precio = capacitacion.Precio,
                Inicio = capacitacion.Inicio,
                Fin = capacitacion.Fin,
                Estado = capacitacion.Estado,
                IDLugar = capacitacion.IDLugar,
                Lugares = lugares
            };
            return View(model);
        }

        // POST: Instructor/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateCapacitacionCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Capacitacion Editada";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoCapacitacionModel()
                {
                    Duracion = model.Duracion,
                    Maximo = model.Maximo,
                    Minimo = model.Minimo,
                    Precio = model.Precio,
                    Inicio = model.Inicio,
                    Fin = model.Fin,
                    Estado = model.Estado,
                    IDLugar = model.IDLugar,
                };
                return View(modelReturn);
            }
        }

        public ActionResult Delete(int id)
        {
            var capacitacion = _capacitacionesQuery.GetCapacitacion(id);
            var lugares = _lugaresQuery.ListLugares();
            var model = new NuevoCapacitacionModel()
            {
                Duracion = capacitacion.Duracion,
                Maximo = capacitacion.Maximo,
                Minimo = capacitacion.Minimo,
                Precio = capacitacion.Precio,
                Inicio = capacitacion.Inicio,
                Fin = capacitacion.Fin,
                Estado = capacitacion.Estado,
                IDLugar = capacitacion.IDLugar,
                Lugares = lugares
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(DeleteCapacitacionCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Capacitacion Elimninado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var model2 = new NuevoCapacitacionModel()
                {
                    Duracion = model.Duracion,
                    Maximo = model.Maximo,
                    Minimo = model.Minimo,
                    Precio = model.Precio,
                    Inicio = model.Inicio,
                    Fin = model.Fin,
                    Estado = model.Estado,
                    IDLugar = model.IDLugar,
                    
                };
                return View(model2);
            }
        }

        public ActionResult Temas(int id)
        {
            var capacitacion = _capacitacionesQuery.GetCapacitacion(id);
            var temas = _temasQuery.ListTemasCapacitacion(id);
            var lugar = _lugaresQuery.GetLugar(capacitacion.IDLugar);
            var model = new CapacitacionTemaIndexModel()
            {
                Capacitaciones = capacitacion,
                Temas = temas,
                Lugares = lugar,
            };
            return View(model);
        }

        public ActionResult NewTema(int id)
        {
            var capacitacion = _capacitacionesQuery.GetCapacitacion(id);
            var temas = _temasQuery.ListTemas();
            var lugar = _lugaresQuery.GetLugar(capacitacion.IDLugar);

            var model = new NuevoCapacitacionTemaModel()
            {
                
                    IDCapacitacion = id,
                    Temas = temas,
                    Capacitacion = lugar.Calle+", "+lugar.Numero+", "+lugar.Depto+", "+lugar.Piso
            };
            return View(model);

        }

        // POST: cAPACITACIONES/New
        [HttpPost]
        public async Task<ActionResult> NewTema(NuevoCapacitacionTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Agregado el Tema a la Capacitacion";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoCapacitacionTemaModel()
                {
                    IDTema = model.IDTema,
                    IDCapacitacion = model.IDCapacitacion,
                   
                };
                return View(modelReturn);
            }


        }
        public ActionResult DeleteTema(int id, int capacitacion)
        {
            var capacitacionBuscada = _capacitacionesQuery.GetCapacitacion(capacitacion);
            var temabuscado = _temasQuery.GetTema(id);
            var lugar = _lugaresQuery.GetLugar(capacitacionBuscada.IDLugar);

            var model = new NuevoCapacitacionTemaModel()
            {
                NombreTema = temabuscado.Nombre + " " + temabuscado.Nivel,
                IDCapacitacion = capacitacion,
                Capacitacion = lugar.Calle + ", " + lugar.Numero + ", " + lugar.Depto + ", " + lugar.Piso,
                IDTema = id,
                
            };
            return View(model);
        }

        // POST: Instructor/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteTema(DeleteCapacitacionTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Tema de la Capacitacion Eliminado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var model2 = new NuevoInstructorTemaModel();
                {

                };
                return View(model2);
            }
        }

    }
}