﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class InstructorIndexModel
    {
        public string Titulo { get; set; }

        public List<InstructorQueryModel> Instructores { get; set; }

    }
}