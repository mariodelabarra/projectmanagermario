﻿using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices.CapacitacionServices
{
    public interface ICapacitacionesQuery
    {
        CapacitacionQueryModel GetCapacitacion(int id);
        List<CapacitacionQueryModel> ListCapacitaciones();
    }
}
