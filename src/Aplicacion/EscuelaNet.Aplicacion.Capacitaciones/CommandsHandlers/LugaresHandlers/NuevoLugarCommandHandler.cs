﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers
{
    public class NuevoLugarCommandHandler : IRequestHandler<NuevoLugarCommand, CommandRespond>
    {
        private ILugarRepository _lugarRepository;
        public NuevoLugarCommandHandler(ILugarRepository lugarRepository)
        {
            _lugarRepository = lugarRepository;
        }

        public Task<CommandRespond> Handle(NuevoLugarCommand request, CancellationToken cancellationToken)
        {

            var responde = new CommandRespond();
            if (request.Capacidad > 0)
            {
                if (string.IsNullOrEmpty(request.Depto))
                {
                    request.Depto = " ";
                }

                if (string.IsNullOrEmpty(request.Piso))
                {
                    request.Piso = " ";
                }

                _lugarRepository.Add(new Lugar(request.Capacidad, request.Calle, request.Numero
                    , request.Depto, request.Piso, request.Localidad, request.Provincia, request.Pais));

                _lugarRepository.UnitOfWork.SaveChanges();
                responde.Succes = true;
                return Task.FromResult(responde);
            }
            else
            {
                responde.Succes = false;
                responde.Error = "La Capacidad debe ser mayor a 0";
                return Task.FromResult(responde);
            }

        }
    }
}
