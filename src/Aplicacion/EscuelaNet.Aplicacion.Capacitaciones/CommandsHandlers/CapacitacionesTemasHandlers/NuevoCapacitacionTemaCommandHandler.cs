﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionTemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorTemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.CapacitacionesTemasHandlers
{
    public class NuevoCapacitacionTemaCommandHandler : IRequestHandler<NuevoCapacitacionTemaCommand, CommandRespond>
    {
        private ICapacitacionRepository _capacitacionRepository;
        private ITemaRepository _temaRepository;

        public NuevoCapacitacionTemaCommandHandler(ICapacitacionRepository capacitacionRepository, ITemaRepository temaRepository)
        {
            _capacitacionRepository = capacitacionRepository;
            _temaRepository = temaRepository;
        }
        public Task<CommandRespond> Handle(NuevoCapacitacionTemaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            var capacitacion = _capacitacionRepository.GetCapacitacion(request.IDCapacitacion);
            var tema = _temaRepository.GetTema(request.IDTema);
            capacitacion.PushTema(tema);

            _capacitacionRepository.Update(capacitacion);

            _capacitacionRepository.UnitOfWork.SaveChanges();

            responde.Succes = true;
            return Task.FromResult(responde);

        }
    }
}
