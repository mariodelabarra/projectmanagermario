﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class DeleteEquipoCommandHandler : IRequestHandler<DeleteEquipoCommand, CommandRespond>
    {
        private IEquipoRepository _equipoRepositorio;
        public DeleteEquipoCommandHandler(IEquipoRepository equipoRepositorio)
        {
            _equipoRepositorio = equipoRepositorio;
        }
        public Task<CommandRespond> Handle(DeleteEquipoCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            
                try
                {
                    var equipo = _equipoRepositorio.GetEquipo(request.Id);
                    _equipoRepositorio.Delete(equipo);
                    _equipoRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);

                }

        }
    }
}
