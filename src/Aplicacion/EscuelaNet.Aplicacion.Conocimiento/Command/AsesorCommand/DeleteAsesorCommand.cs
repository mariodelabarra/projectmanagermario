﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.Command.AsesorCommand
{
    public class DeleteAsesorCommand : IRequest<CommandRespond>
    {
        public int IDConocimiento { get; set; }

        public int IDAsesor { get; set; }
    }
}
