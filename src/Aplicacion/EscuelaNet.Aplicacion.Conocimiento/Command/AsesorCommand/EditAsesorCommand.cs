﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.Command.AsesorCommand
{
    public class EditAsesorCommand : IRequest<CommandRespond>
    {
        public int IdAsesor { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Idioma { get; set; }
        public Demanda Demanda { get; set; }
        public string Pais { get; set; }
    }
}
