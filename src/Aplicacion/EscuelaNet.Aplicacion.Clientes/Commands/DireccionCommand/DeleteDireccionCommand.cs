﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.Commands.DireccionCommand
{
    public class DeleteDireccionCommand : IRequest<CommandRespond>
    {
       
        public int IdUnidad { get; set; }

        public int IdDireccion { get; set; }

        public string Domicilio { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public string Pais { get; set; }

    }
}
