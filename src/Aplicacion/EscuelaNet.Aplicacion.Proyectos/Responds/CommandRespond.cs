﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.Responds
{
    public class CommandRespond
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }
}
