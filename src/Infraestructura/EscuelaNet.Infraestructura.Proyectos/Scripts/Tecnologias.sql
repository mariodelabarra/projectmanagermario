USE [Proyectos]
GO

/****** Object:  Table [dbo].[Tecnologias]    Script Date: 18/09/2019 11:28:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tecnologias](
	[IDTecnologia] [int] NOT NULL,
	[nombre] [varchar](120) NOT NULL,
 CONSTRAINT [PK_Tecnologias] PRIMARY KEY CLUSTERED 
(
	[IDTecnologia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


