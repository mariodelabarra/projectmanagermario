﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.EntityTypeConfigurations
{
    public class ConocimientosEntityTypeConfiguration : EntityTypeConfiguration<Conocimiento>
    {
        public ConocimientosEntityTypeConfiguration()
        {
            this.ToTable("Conocimiento");
            this.HasKey<int>(con => con.ID);
            this.Property(con => con.ID)
                .HasColumnName("conocimiento_ID");
            this.Property(con => con.Nombre)
                 .IsRequired();
            this.Property(con => con.Demanda)
                .HasColumnName("Demanda");
            this.Property(un => un.IDCategoria)
                .IsRequired();

            //Conocimiento tiene muchos Asesores y Asesores tiene muchos Conocimientos
            this.HasMany<Asesor>(un => un.Asesores)
                .WithMany(s => s.Conocimientos)
                .Map(us =>
                {
                    us.MapRightKey("IDAsesor");
                    us.MapLeftKey("conocimiento_ID");
                    us.ToTable("AsesorConocimiento");
                });
        }
    }
}
