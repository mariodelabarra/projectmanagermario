﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Alumno : Entity
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }

        [DisplayName("Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }
        private Alumno()
        {

        }
        public Alumno(string nombre,string apellido,string dni, DateTime fechaNacimiento) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Dni = dni ?? throw new System.ArgumentNullException(nameof(nombre));
            this.FechaNacimiento = fechaNacimiento;
        }

    }
}